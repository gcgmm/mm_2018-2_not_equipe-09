﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerOptions : MonoBehaviour {


    MenuOptions m_MenuOptions;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OptionAButtonPressed()
    {
        m_MenuOptions = FindObjectOfType<MenuOptions>();
        if (m_MenuOptions && !m_MenuOptions.IsDisplayed)
        {
            m_MenuOptions.ShowOptionsMenu(true);
        }
        Debug.Log("OptionAButtonPressed() called.");

        //if (SceneManager.GetActiveScene().name != m_BackButtonNavigation)
        //   LoadScene(m_BackButtonNavigation);
    }
}
 