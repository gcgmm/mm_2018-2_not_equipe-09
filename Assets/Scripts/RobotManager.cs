﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotManager : MonoBehaviour {
    
    void Awake()
    {
        createObject("Cubo", new Vector3(0, 0, 0), PrimitiveType.Cube, new Vector3(0.5f, 1, 0.5f));
    }

    GameObject createObject(string name, Vector3 position, UnityEngine.PrimitiveType primitiveType, Vector3 scale, string parent = "ImageTarget")
    {
        GameObject dynamicObject = GameObject.CreatePrimitive(primitiveType);
        dynamicObject.name = name;
        dynamicObject.transform.localScale = scale;
        dynamicObject.transform.position = position;
        dynamicObject.transform.SetParent(GameObject.Find(parent).transform);
        return dynamicObject;
    }
}
