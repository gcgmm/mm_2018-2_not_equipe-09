﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Caindo : MonoBehaviour
{    
    private bool comecaJogo = false;
    float lerpPosition = 0.0f;
    public float lerpTime = 3.7f;
    public Vector3 start;// = new Vector3(-0.117f, 1.542f, 0.513f);
    public Vector3 final;// = new Vector3(-0.117f, 0.044f, -0.389f);
    public Vector3 mais;// = new Vector3(-0.117f, 0.044f, -0.589f);
    public Vector3 menos;// = new Vector3(-0.117f, 0.044f, -0.189f);

    // Use this for initialization
    void Start()
    {
        //GameVariables.tipoQueda = TipoQueda.Menos;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameVariables.target && GameVariables.cair)
        {
            comecaJogo = true;
        }
        if (comecaJogo)
        {
            switch (GameVariables.tipoQueda)
            {
                case TipoQueda.Sucesso:
                    lerpPosition += Time.deltaTime / lerpTime;
                    transform.position = Vector3.Lerp(start, final, lerpPosition);
                    break;
                case TipoQueda.Mais:
                    lerpPosition += Time.deltaTime / lerpTime;
                    transform.position = Vector3.Lerp(start, mais, lerpPosition);
                    break;
                case TipoQueda.Menos:
                    lerpPosition += Time.deltaTime / lerpTime;
                    transform.position = Vector3.Lerp(start, menos, lerpPosition);
                    break;
            }
            if(transform.position == final
                || transform.position == mais
                || transform.position == menos)
            {
                GameVariables.cair = false;
                comecaJogo = false;
                transform.position = start;
                lerpPosition = 0.0f;
            }
        }
    }
}
