﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    AudioSource audioData;
    public AudioClip ambientSound;
    public AudioClip rightAnswerSound;
    public AudioClip wrongAnswerSound;
    // Use this for initialization
    void Start()
    {
        audioData = GetComponent<AudioSource>();
        audioData.clip = ambientSound;
        audioData.Play(0);
        audioData.loop = true;
        //audioData.Stop();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
