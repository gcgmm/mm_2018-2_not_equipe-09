﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotBehaviour2 : MonoBehaviour {


    private Rigidbody rb;
    public float speed = 10f;

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody>();
        float moveHorizontal = 10f;//Input.GetAxis("Horizontal");
        float moveVertical = 10f;// Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}