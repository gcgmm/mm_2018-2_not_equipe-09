﻿/*===============================================================================
Copyright (c) 2018 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

using UnityEditor;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationHandler : MonoBehaviour
{
    #region PUBLIC_MEMBERS
    public string m_BackButtonNavigation = "[Name of Scene To Load]";
    #endregion // PUBLIC_MEMBERS

    MenuOptions m_MenuOptions;
    MenuOptionsCorrect m_MenuOptionsCorrect;
    CameraSettings cameraSettings;
    bool isOn = false;

    AudioSource ambientAudioData;
    AudioSource responseAudioData;
    public AudioClip ambientSound;
    public AudioClip rightAnswerSound;
    public AudioClip wrongAnswerSound;

    #region MONOBEHAVIOUR_METHODS
    void Update()
    {
        // On Android, the Back button is mapped to the Esc key
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            HandleBackButtonPressed();
        }
    }
    void Start()
    {

        ambientAudioData = GameObject.Find("AmbientSound").GetComponent<AudioSource>();
        responseAudioData = GameObject.Find("ResponseSound").GetComponent<AudioSource>();
        ambientAudioData.clip = ambientSound;
        ambientAudioData.Play(0);
        ambientAudioData.loop = true;
        ambientAudioData.volume = 0.2f;
        //responseAudioData.loop = false;


        m_MenuOptions = FindObjectOfType<MenuOptions>();
        m_MenuOptionsCorrect = FindObjectOfType<MenuOptionsCorrect>();
    }
    #endregion // MONOBEHAVIOUR_METHODS


    #region PUBLIC_METHODS
    public void HandleBackButtonPressed()
    {
        if (m_MenuOptions && !m_MenuOptions.IsDisplayed)
        {
            m_MenuOptions.ShowOptionsMenu(true);
        }
        Debug.Log("HandleBackButtonPressed() called.");
        //if (SceneManager.GetActiveScene().name != m_BackButtonNavigation)
        //    LoadScene(m_BackButtonNavigation);
    }

    public void OptionAButtonPressed()
    {
        cameraSettings = FindObjectOfType<CameraSettings>();
        cameraSettings.SwitchFlashTorch(!isOn);
        isOn = !isOn;
        /* m_MenuOptions = FindObjectOfType<MenuOptions>();
         if (m_MenuOptions && !m_MenuOptions.IsDisplayed)
         {
             m_MenuOptions.ShowOptionsMenu(true);
         }
         Debug.Log("OptionAButtonPressed() called.");
         */
        // if (SceneManager.GetActiveScene().name != m_BackButtonNavigation)
        //     LoadScene(m_BackButtonNavigation);
    }
    public void OptionBButtonPressed()
    {
        if (m_MenuOptions && !m_MenuOptions.IsDisplayed)
        {
            m_MenuOptions.ShowOptionsMenu(true);
        }
        /* m_MenuOptions = FindObjectOfType<MenuOptions>();
         if (m_MenuOptions && !m_MenuOptions.IsDisplayed)
         {
             m_MenuOptions.ShowOptionsMenu(true);
         }
         Debug.Log("OptionAButtonPressed() called.");
         */
        // if (SceneManager.GetActiveScene().name != m_BackButtonNavigation)
        //     LoadScene(m_BackButtonNavigation);
    }
    public void OptionCButtonPressed()
    {
        cameraSettings = FindObjectOfType<CameraSettings>();
        cameraSettings.SwitchFlashTorch(!isOn);
        isOn = !isOn;
    }

    public void Option1AButtonPressed()
    {
        m_MenuOptions.ShowOptionsMenu(true);
        PlayWrongSound();
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Menos;
    }

    public void Option1BButtonPressed()
    {
        m_MenuOptions.ShowOptionsMenu(true);
        PlayWrongSound();
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Mais;
    }

    public void Option1CButtonPressed()
    {
        //m_MenuOptionsCorrect.ShowOptionsMenu(true);
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Sucesso;
        PlayCorrectSound();
        //yield return new WaitForSeconds(5);
        StartCoroutine(OpenScene("3-Question2"));
        //cameraSettings = FindObjectOfType<CameraSettings>();
        //cameraSettings.SwitchFlashTorch(!isOn);
        //isOn = !isOn;
    }

    public void Option2AButtonPressed()
    {
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Menos;
        PlayWrongSound();
        m_MenuOptions.ShowOptionsMenu(true);
    }

    public void Option2BButtonPressed()
    {
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Sucesso;
        PlayCorrectSound();
        StartCoroutine(OpenScene("3-Question3"));
    }

    public void Option2CButtonPressed()
    {
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Mais;
        PlayWrongSound();
        m_MenuOptions.ShowOptionsMenu(true);
    }

    public void Option3AButtonPressed()
    {
        PlayCorrectSound();
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Sucesso;
        StartCoroutine(OpenScene("4-Fim"));
    }

    IEnumerator OpenScene(string scene)
    {
        yield return new WaitForSeconds(5);
        GameVariables.target = false;
        SceneManager.LoadSceneAsync(scene);

    }

    public void Option3BButtonPressed()
    {
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Mais;
        PlayWrongSound();
        m_MenuOptions.ShowOptionsMenu(true);
    }

    public void Option3CButtonPressed()
    {
        GameVariables.cair = true;
        GameVariables.tipoQueda = TipoQueda.Menos;
        PlayWrongSound();
        m_MenuOptions.ShowOptionsMenu(true);
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS
    void PlayCorrectSound()
    {
        responseAudioData.clip = rightAnswerSound;
        responseAudioData.Play(0);
    }

    void PlayWrongSound()
    {
        responseAudioData.clip = wrongAnswerSound;
        responseAudioData.Play(0);
    }

    void LoadScene(string sceneName)
    {
        Debug.Log("LoadScene(" + sceneName + ") called.");
        if (!string.IsNullOrEmpty(sceneName))
        {
            SceneManager.LoadScene(sceneName);
        }
    }
    #endregion // PRIVATE_METHODS
}
